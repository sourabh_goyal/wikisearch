package all_main;

import java.util.Scanner;

import search.Search;

public class SearchMain {

	private static Scanner in;
		public static void main(String args[]){
		
		String uri;
		try{
			uri = "C:\\Users\\Sourabh\\workspace\\Search\\resources";
			System.out.println("Initializing engine, please wait....\n");
			Search search = new Search(uri);
			System.out.println("Engine Intialization completed\n");
			String query;
			while(true)
			{
				in = new Scanner(System.in);
				System.out.print("What would you like to search for?\n");
				query = in.nextLine();
				System.out.println("You searched for: " + query);
				double startTime = System.currentTimeMillis();
				search.searchFunc(query);
				double endTime   = System.currentTimeMillis();
				double totalTime = endTime - startTime;
				System.out.println("hurray!! search results completed in : "+ totalTime/1000 + " sec");

			}
		}
		catch(Exception e){
			System.out.println("OOPS! Something unexpected happend");
			e.printStackTrace();
		}
		
		
	}	

}
