package all_main;

import parser.ParsingXML;
import secondaryIndex.SecondaryIndex;
import merger.Merger;

public class IndexMain {
	public static void main(String args[])
	{
		//String uri = "C:\\enwiki\\enwiki.xml";
		//String uri1 = "C:\\enwiki\\output";
		String uri = new String();
		String uri1 = new String();
		try{
			uri = args[0];
			uri1 = args[1];
		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println("EXCEPTION : Command Line Arguments are Missing.");
			return;
		}
		
		ParsingXML parse = new ParsingXML();
		long fnum = 0;
//		
		double startTime = System.currentTimeMillis();
//		
		fnum = parse.parseMethod(uri,uri1);
		
	//	System.out.println(fnum);
	    //System.out.println("MERGING of " + fnum + " FILES STARTS....");
//		
		if(fnum > 0){
			Merger mg = new Merger();
			mg.mergingFunc(uri1, fnum);
		}
////		
		//SECONDARY INDEX
		System.out.println("Mergin Compeletes.!!!!   Secondary Index starts....");
		SecondaryIndex secInd = new SecondaryIndex();
		secInd.createSecIndex(uri1,1);	//Secondary Index of main Index
		secInd.createSecIndex(uri1,2);	//Secondary Index of mapping file Index
		
		
		double endTime   = System.currentTimeMillis();
		double totalTime = endTime - startTime;
		System.out.println("RUNNING TIME FOR INDEX CREATION : "+ totalTime/1000 + " sec");
		//deleteDirectory(new File(uri1+"\\FILE"));
		//System.out.println(true);
		return;
		
	}
}
