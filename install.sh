#!/bin/sh
n=$#
if [ $n -ne 2 ]
then
echo "Error!please provide sample data path"
exit 1
fi
if [ -e "$1" ]
then
if [ ! -d "bin" ]
then
mkdir bin
else
rm -rf bin
mkdir bin
fi
fi
#Compiling Files
echo "Compiling Source..."
javac -cp lib/sax.jar:lib/porter-stemmer-1.4.jar -d bin -sourcepath src  src/all_main/IndexMain.java
javac -cp lib/*.jar:lib/porter-stemmer-1.4.jar -d bin -sourcepath src  src/all_main/SearchMain.java
echo "All Source Compiled Successfully"
echo "Run Indexing as Part of Installation"
java -cp lib/sax.jar:lib/porter-stemmer-1.4.jar:bin all_main.IndexMain "$1" "$2" 
